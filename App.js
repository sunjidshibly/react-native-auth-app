import React from 'react';
import AppNavigation from "./src/AppNavigation";
import * as firebase from 'firebase';


export default class App extends React.Component {
    componentWillMount() {
        const firebaseConfig = {
            apiKey: "AIzaSyAxZFEhp9WnJodDZTtji8vPc9f4f0qgsK0",
            authDomain: "reactnativeauthapp-8694a.firebaseapp.com",
            databaseURL: "https://reactnativeauthapp-8694a.firebaseio.com",
            projectId: "reactnativeauthapp-8694a",
            storageBucket: "reactnativeauthapp-8694a.appspot.com",
            messagingSenderId: "398271274804",
            appId: "1:398271274804:web:a16235c930b605cf"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    }

    render() {
        return <AppNavigation/>;
    }
}

