import React, {Component} from 'react'
import {Text, View, TextInput} from 'react-native'

export default class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {
            borderColor: '#9999',
            borderWidth: 1,
            backgroundColor: 'white'
        }
    }

    handleFocus = () => {
        this.setState({
            borderColor: '#8D02CE',
            borderWidth: 2,
            backgroundColor: '#8d02ce0a'
        });
        if (this.props.onFocus) {
            this.props.onFocus();
        }
    };


    handleBlur = () => {

        this.setState({
            borderColor: '#9999',
            borderWidth: 1,
            backgroundColor: 'white'
        });
        if (this.props.onBlur) {
            this.props.onBlur();
        }

    }

    render() {
        const {placeholder, handleInput, secureTextEntry, customStyle, keyboardType, error} = this.props;
        const {borderColor, borderWidth, backgroundColor} = this.state;

        return (
            <View style={{flex: 1, marginBottom: 20,}}>
                <TextInput
                    style={{
                        height: 50,
                        borderRadius: 8,
                        padding: 10,
                        borderColor,
                        backgroundColor,
                        borderWidth,
                    }}
                    placeholder={placeholder}
                    onChangeText={handleInput}
                    autoCorrect={false}
                    autoCapitalize={'none'}
                    keyboardType={keyboardType || 'default'}
                    secureTextEntry={secureTextEntry}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                />

                {error && <Text style={{color: 'red', fontSize: 12, marginTop: 5}}>{error}</Text>}

            </View>

        )
    }
}