import React, {Component} from 'react';
import {View, Text} from 'react-native';
import * as firebase from 'firebase';
import Button from "../components/common/Button";

class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users_info: null,
        }

    }

    componentWillMount() {
        const userId = firebase.auth().currentUser.uid;
        firebase.database().ref('/users/' + userId).once('value').then(function (snapshot) {
            const userinfo = snapshot.val();
            // return userinfo
            console.log(userinfo);
            this.setState({
                users_info: userinfo,
            });
            console.log(this.state.users_info);
        });
    }

    Logout = () => {
        firebase.auth().signOut();
        this.props.navigation.replace('Landing');
    };

    render() {
        return (
            <View>
                <Text>Profile Page</Text>
                <Button title="Logout"
                        backgroundColor="red"
                        onButtonPress={() => this.Logout()}

                />
            </View>
        );
    }
}

export default ProfileScreen;