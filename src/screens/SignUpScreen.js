import React, {Component} from 'react';
import {Text, View, ScrollView, ActivityIndicator, Alert} from 'react-native';
import Input from '../components/common/Input';
import Button from '../components/common/Button';
import * as firebase from 'firebase';

export default class SignUpScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            gender: '',
            age: '',
            firstName: '',
            lastName: '',
            emailError: null,
            passError:null,
            loading: false,
            ageError:null,
            genderError:null,
            lastNameError:null,
            firstNameError:null,
        };
    }

    handleInput = (key, value) => {
        this.setState({
            [key]: value,
        });
    };

    checkGerderValidity = ()=> {
        const {gender} = this.state;
        const isValid = (gender.toLocaleLowerCase() === 'male' ||gender.toLocaleLowerCase() === 'female')?true:false;
        return isValid;
    }

    checkGender =()=>{
        const isValid= this.checkGerderValidity();
        if (!isValid) {
            this.setState({
                genderError: 'Gender must be male or female',
            });
        } else {
            this.setState({
                genderError: null,
            });
        }
    }

    lastNameValidity = ()=>{
        const {lastName} = this.state;
        const isValid = ( lastName.trim().length === 0)?false:true;
        return isValid;
    }

    checkLastName = ()=>{
        const isValid = this.lastNameValidity();
        if (!isValid) {
            this.setState({
                lastNameError: 'Invalid Last Name',
            });
        } else {
            this.setState({
                lastNameError: null,
            });
        }

    }

    checkFirstName = ()=>{
        const isValid = this.firstNameValidity();
        if (!isValid) {
            this.setState({
                firstNameError: 'Invalid First Name',
            });
        } else {
            this.setState({
                firstNameError: null,
            });
        }

    }

    firstNameValidity = ()=>{
        const {firstName} = this.state;
        const isValid = ( firstName.trim().length === 0)?false:true;
        return isValid;
    }

    checkAgeValidity= ()=>{
        const {age} = this.state;
        const isValid = (parseInt(age) > 0 && parseInt(age) < 101)?true:false;
        return isValid;
    }

    checkAge = () =>{
        const isValid = this.checkAgeValidity();
        if (!isValid) {
            this.setState({
                ageError: 'Age must be within one to hundred years',
            });
        } else {
            this.setState({
                ageError: null,
            });
        }
    }

    checkMailPattern = ()=>{
        const {email} = this.state;
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const isValid = re.test(String(email).toLowerCase());
        return isValid;
    };

    checkEmail = () => {
        const isValid = this.checkMailPattern();
        if (!isValid) {
            this.setState({
                emailError: 'Invalid Email',
            });
        } else {
            this.setState({
                emailError: null,
            });
        }
    };

    checkPasswordPattern = ()=>{
        const {password } = this.state;
        const re = /^(?=.*\d)(?=.*[A-Z]).{6,20}$/;
        const isValid = re.test(String(password));
        return isValid;
    }
    checkPassword = () =>{
        const isValid = this.checkPasswordPattern();
        if (!isValid) {
            this.setState({
                passError: 'Password should be 6-20 characters with at least one numeric, one uppercase.',
            });
        } else {
            this.setState({
                passError: null,
            });
        }
    };

    AlertMessage = (message) => {
        this.setState({loading: false});
        Alert.alert(
            message.code,
            message.message,
            [
                {text: 'OK'},
            ],
            {cancelable: false},
        );
    };
    signUpUser = () => {
        let {email, password, firstName, lastName, age, gender} = this.state;
        age = parseInt(age);
        if(!(this.checkMailPattern())) {
            this.AlertMessage({code:'Error',message:'Invalid Email'});
            return;
        }
        if(!(this.checkPasswordPattern())){
            this.AlertMessage({code:'Error',message:'Password should be 6-20 characters with at least one numeric, one uppercase.'});
            return
        }
        if(!(this.firstNameValidity())){
            this.AlertMessage({code:'Error',message:'Invalid First Name'});
            return
        }
        if(!(this.lastNameValidity())){
            this.AlertMessage({code:'Error',message:'Invalid Last Name'});
            return
        }
        if(!(this.checkAgeValidity())){
            this.AlertMessage({code:'Error',message:'Age must be within one to hundred years'});
            return
        }
        if(!(this.checkGerderValidity())){
            this.AlertMessage({code:'Error',message:'Gender must be male or female'});
            return
        }

        this.setState({loading: true});
        firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then(user => {
                const userID = user.user.uid;
                firebase
                    .database()
                    .ref('users/' + userID)
                    .set({
                        firstName,
                        lastName,
                        age,
                        gender,
                        email,
                    });
                this.props.navigation.replace('Profile');
            }).catch(error => this.AlertMessage(error));

    };

    render() {
        const {loading} = this.state;

        if (loading) {
            return (
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#0000ff"/>
                </View>
            )
        }
        return (
            <ScrollView style={{flex: 1}}>
                <View style={{margin: 25}}>
                    <Input
                        handleInput={text => this.handleInput('email', text)}
                        onBlur={this.checkEmail}
                        placeholder="Email"
                        error={this.state.emailError}
                    />

                    <Input
                        handleInput={text => this.handleInput('password', text)}
                        onBlur={this.checkPassword}
                        secureTextEntry={true}
                        placeholder="6-20 characters password with one numeric, one uppercase"
                        error={this.state.passError}
                    />

                    <View style={{flexDirection: 'row'}}>
                        <Input
                            containerStyle={{flex: 1}}
                            customStyle={{flex: 1, marginRight: 5}}
                            handleInput={text => this.handleInput('firstName', text)}
                            placeholder="First Name"
                            onBlur = {this.checkFirstName}
                            error={this.state.firstNameError}
                        />
                        <Input
                            containerStyle={{flex: 1}}
                            customStyle={{flex: 1}}
                            handleInput={text => this.handleInput('lastName', text)}
                            placeholder="Last Name"
                            onBlur = {this.checkLastName}
                            error={this.state.lastNameError}
                        />
                    </View>

                    <View style={{flexDirection: 'row'}}>
                        <Input
                            containerStyle={{flex: 1}}
                            customStyle={{flex: 1, marginRight: 5}}
                            handleInput={text => this.handleInput('age', text)}
                            onBlur={this.checkAge}
                            keyboardType='numeric'
                            placeholder="Age"
                            error={this.state.ageError}
                        />
                        <Input
                            containerStyle={{flex: 1}}
                            customStyle={{flex: 1}}
                            handleInput={text => this.handleInput('gender', text)}
                            placeholder="Gender"
                            onBlur={this.checkGender}
                            error={this.state.genderError}
                        />
                    </View>

                    <Button onButtonPress={this.signUpUser} title="Sign Up"/>
                </View>
            </ScrollView>
        );
    }
}
