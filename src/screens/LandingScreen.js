import React, {Component} from 'react';
import {View, Text, ActivityIndicator, Alert, ImageBackground} from 'react-native';
import Button from "../components/common/Button";
import * as firebase from 'firebase';
import {Facebook} from 'expo';

class LandingScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,

        }
    }

    static navigationOptions = {
        header: null,
    };

    componentWillMount() {
        this.checkUserStatus();
    }

    checkUserStatus = ()=>{
        firebase.auth().onAuthStateChanged((user) => {
            if (user != null) {
                this.props.navigation.navigate('Profile');
            } else {
                this.setState({loading: false});
            }
        });
    }

    AlertMessage = (message) => {
        this.setState({loading: false});
        Alert.alert(
            message.code,
            message.message,
            [
                {text: 'OK'},
            ],
            {cancelable: false},
        );
    };

    async FacebookLogin() {
        const {type, token} = await Facebook.logInWithReadPermissionsAsync('2262424380636159', {
            permission: 'public_profile'
        });
        if (type == 'success') {
            this.setState({loading: true});
            const credintial = firebase.auth.FacebookAuthProvider.credential(token);
            const facebookProfile = await firebase.auth().signInWithCredential(credintial).catch(error => this.AlertMessage(error));

            const userID = facebookProfile.user.uid;
            firebase
                .database()
                .ref('users/' + userID)
                .set({
                    firstName:facebookProfile.additionalUserInfo.profile.first_name,
                    lastName:facebookProfile.additionalUserInfo.profile.last_name,
                    photo:facebookProfile.additionalUserInfo.profile.picture.data.url,
                    email: facebookProfile.user.email,
                }).catch(error => this.AlertMessage(error));

            // Listen for authentication state to change.
            this.checkUserStatus();

        }

    }

    render() {
        const {loading} = this.state;

        if (loading) {
            return (
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#0000ff"/>
                </View>
            )
        }
        return (
            <ImageBackground
                source={require('../../assets/auth.gif')}
                // style={{width: '100%', height: '100%'}}
                style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
            >
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Button title="Login with Email"
                            backgroundColor="orange"
                            onButtonPress={() => this.props.navigation.navigate('SignIn')}
                    />
                    <Button title="Login with Facebook"
                            backgroundColor="blue"
                            onButtonPress={() => this.FacebookLogin()}
                    />
                    <Button title="Sign Up"
                            onButtonPress={() => this.props.navigation.navigate('SignUp')}
                    />
                </View>
            </ImageBackground>
        );
    }
}

export default LandingScreen;