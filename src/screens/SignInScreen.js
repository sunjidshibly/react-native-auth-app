import React, {Component} from 'react';
import {View, Text, ScrollView, ActivityIndicator, Alert} from 'react-native';
import Input from "../components/common/Input";
import Button from "../components/common/Button";
import * as firebase from 'firebase';

class SignInScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            email: '',
            password: '',
            emailError: null,
            passError:null,
        }
    }

    handleInput = (key, value) => {
        this.setState({
            [key]: value,
        });
    };

    checkMailPattern = ()=>{
        const {email} = this.state;
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const isValid = re.test(String(email).toLowerCase());
        return isValid;
    };

    checkEmail = () => {
        const isValid = this.checkMailPattern();
        if (!isValid) {
            this.setState({
                emailError: 'Invalid Email',
            });
        } else {
            this.setState({
                emailError: null,
            });
        }
    };

    checkPasswordPattern = ()=>{
        const {password } = this.state;
        const re = /^(?=.*\d)(?=.*[A-Z]).{6,20}$/;
        const isValid = re.test(String(password));
        return isValid;
    }
    checkPassword = () =>{
        const isValid = this.checkPasswordPattern();
        if (!isValid) {
            this.setState({
                passError: 'Password should be 6-20 characters with at least one numeric, one uppercase.',
            });
        } else {
            this.setState({
                passError: null,
            });
        }
    };

    AlertMessage = (message) => {
        Alert.alert(
            message.code,
            message.message,
            [
                {text: 'OK'},
            ],
            {cancelable: false},
        )
    };

    signInUser = () => {
        if(!(this.checkMailPattern())) {
            this.AlertMessage({code:'Error',message:'Invalid Email'});
            return;
        }
        if(!(this.checkPasswordPattern())){
            this.AlertMessage({code:'Error',message:this.state.passError});
            return
        }

        this.setState({loading: true});
        const {email, password} = this.state;
        firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then(user => {
                if (user) {
                    this.setState({loading: false});
                    this.props.navigation.replace('Profile');
                } else {
                    this.setState({loading: false});
                }
            }).catch(error => {
            this.setState({loading: false});
            this.AlertMessage(error);
        });

    };

    render() {

        const {loading} = this.state;
        if (loading) {
            return (
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#0000ff"/>
                </View>
            )
        }
        return (
            <ScrollView style={{flex: 1}}>
                <View style={{margin: 25}}>
                    <Input
                        handleInput={text => this.handleInput('email', text)}
                        onBlur={this.checkEmail}
                        placeholder="Email"
                        error={this.state.emailError}
                    />

                    <Input
                        handleInput={text => this.handleInput('password', text)}
                        secureTextEntry={true}
                        placeholder="6-20 characters password with one numeric, one uppercase"
                        error={this.state.passError}
                        onBlur={this.checkPassword}
                    />

                    <Button title="Login with Email"
                            backgroundColor="orange"
                            onButtonPress={this.signInUser}
                    />
                </View>
            </ScrollView>
        );
    }
}

export default SignInScreen;