import {createStackNavigator, createAppContainer} from 'react-navigation';
import LandingScreen from './screens/LandingScreen';
import SignUpScreen from './screens/SignUpScreen';
import ProfileScreen from './screens/ProfileScreen';
import SignInScreen from "./screens/SignInScreen";

const AppNavigation = createStackNavigator({
    Landing: {
        screen: LandingScreen
    },
    SignUp: {
        screen: SignUpScreen
    },
    Profile: {
        screen: ProfileScreen
    },
    SignIn: {
        screen: SignInScreen
    }
});

export default createAppContainer(AppNavigation);